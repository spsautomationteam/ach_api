// an EV policy would be much better, but something about the gateway response isnt xpath-friendly

var gatewayResponse = context.getVariable("message.content");

function findInXml(elementName){
    var regExString = "<lolneedle>[0-9a-zA-Z]*<\\/lolneedle>".replace(/lolneedle/g, elementName);
    var regX = new RegExp(regExString);
    var xmlElement = regX.exec(gatewayResponse)[0];
    var xmlValue = xmlElement.replace("<" + elementName + ">", "").replace("</" + elementName + ">", "");
    return xmlValue;
}
try {
    var success = findInXml("SUCCESS");
    var guid = success === "true" ? findInXml("GUID") : "";
    context.setVariable("sage.vault.status", success === "true" ? "1" : "0"); // to match swagger api
    context.setVariable("sage.vault.token", guid);
    
} catch (ex) {
    // for DELETE... hacky, but reeeal rare use case
    var success = findInXml(".*"); 
    context.setVariable("sage.vault.status", success.indexOf(">true<")> -1 ? "1" : "0"); // to match swagger api
}


