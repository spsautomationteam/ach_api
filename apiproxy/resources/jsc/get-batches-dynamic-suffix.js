var verb = context.getVariable("message.verb");
var suffix = context.getVariable("proxy.pathsuffix");
var resource;

// batches/external is the only GET avaiable on /bankcard, and that has its own flow, so this works
switch (verb){
    case "POST":
        resource = "/ach";
        break;
    case "GET":
        resource = "/data";
        break;
    default:
        resource = "";
        break;
}

// at the gateway, a path like: /data/batches/* 
// usually (AM - Skip Scoping) has a scoped equivalent like: /data/{bankcard/ach}/*
var skipScoping = context.getVariable("sage.skipScoping"); // "AM - Skip Scoping"
if (resource === "/data" && !skipScoping){
    suffix = suffix.replace("/batches", "/batches/ach");
}

context.setVariable("sage.suffixWithReference", resource + suffix);
