//# --- GET ---

var mid = context.getVariable("sage.merchantId");
var mkey = context.getVariable("sage.merchantKey");
//Get auth values from KVM sps-configuration
var oauth_id = context.getVariable("sage.oauth_id");
var oauth_key = context.getVariable("sage.oauth_key");

//# target. Get values from KVM sps-configuration
var oauth_host = context.getVariable("sage.oauth_host");
var oauth_path = context.getVariable("sage.oauth_path");

var url = 'https://' + oauth_host + oauth_path ;//+ '/tokens';

var verb = 'POST';
var nonce = hmacTools.nonce(12); 
var body = '{ "grant_type": "client_credentials", "client_id": "' + oauth_id + '"}'; // client credentials grant


//# --- create HMAC ---
var data = verb + url +  nonce + body;
var hmac = hmacTools.createHmac(oauth_key, data);


//# --- SET ---
context.setVariable("sage.oauth-body", body);
context.setVariable("sage.oauth-hmac", "HMAC " + hmac);
context.setVariable("sage.oauth-nonce", nonce);

