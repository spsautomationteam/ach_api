var fullSuffix = context.getVariable("proxy.pathsuffix");
// /logs/123 ==> ["", "logs", "123"]
var suffixArray = fullSuffix.split('/');

context.setVariable("sage.sessionId", suffixArray[2]);
