function setTransactionType(type){
    var requestContent = context.getVariable("request.content");
    var requestObject;
    try {
        requestObject = JSON.parse(requestContent);
    } catch (ex) {
        requestObject = {};
    }
    requestObject.TransactionCode = type.toString();
    context.setVariable("request.content", JSON.stringify(requestObject));
}