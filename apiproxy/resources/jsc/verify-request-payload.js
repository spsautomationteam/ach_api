var requestContent = context.getVariable("request.content");
var requestObject = JSON.parse(requestContent);
var merchId = context.getVariable('sage.merchantId');
var environment = context.getVariable("environment.name");

//print('requestObject: ' + JSON.stringify(requestObject));

//Second level of verification so that transactions are not interupted in Prod.
if (environment != "prod" && requestObject) {
    // print('requestObject: ' + JSON.stringify(requestObject));
    if (requestObject) {
        var checkData = requestObject.Account || requestObject.account;
        //print('checkData: ' + JSON.stringify(checkData));
        if (checkData) {
            var routing = checkData.routingNumber || checkData.RoutingNumber || checkData.routingnumber || checkData.Routingnumber || false;
            var account = checkData.accountNumber || checkData.AccountNumber || checkData.accountnumber || checkData.Accountnumber || false;
            
            if (routing || account) {
                /* jshint ignore:start */
                const testSuites = (function() {
                    var suites = [];
                    routing && suites.push([routing, ['056008849']]);
                    account && suites.push([account, ['12345678901234']]);
                    merchId && suites.push([merchId, ['999999999997', '173859436515']]);
                    return suites;
                })();

                const numPassedTests = (function(suites){
                    var passed = 0;
                    suites.forEach(function(suite){ suite[1].indexOf(suite[0]) > -1 ? passed++ : void 0 });
                    return passed;
                })(testSuites);
                /* jshint ignore:end */
    
                if (numPassedTests != testSuites.length) {
                    context.setVariable("sage.invalid.certpayload", true);
                }
            }
        }
    }
}