var fullSuffix = context.getVariable("proxy.pathsuffix");
// /charges/123 ==> ["", "charges", "123"]
var suffixArray = fullSuffix.split('/');

// could do some assertions here but ultimately this policy should only be used in
// conditional flows that matchpath foo/*, not foo/** . See MatchesPath docs

// cant concatenate these in an AM/AV node
// http://stackoverflow.com/questions/22821118/using-assignmessage-policy-to-generate-a-formatted-string-in-apigee
context.setVariable("sage.suffixWithReference", "/data/transactions/" + suffixArray[2]);
