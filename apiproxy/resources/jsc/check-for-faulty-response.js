var infoTemplate = "https://developer.sagepayments.com/docs/errors#";

try {
    var respString = context.getVariable("message.content");
    var response = JSON.parse(respString);

    // Set standard sage variables for AM default: status.code, reason.phrase, error.code, error.message(KVM), error.info, error.detail
    if (response.fault) {
        // Apigee policy fault (e.g. InvalidAPIKey) is caught in FaultRules and set variables set by corresponding AM - ...
        //print("APIGEE POLICY FAULT");
        // Next 2 are set by AM-... via variables
        context.setVariable("message.status.code", context.getVariable("sage.error.status"));
        context.setVariable("message.reason.phrase", context.getVariable("sage.error.reason"));
        context.setVariable("sage.error.info", infoTemplate + context.getVariable("sage.error.code"));
        context.setVariable("sage.error.detail", JSON.stringify(context.getVariable("sage.error.detail")));
    }
    else if (response.detail && response.code) {
        //print("MANUALLY RAISED by Raise Fault");
        // Next 2 are set by RF-... via StatusCode and ReasonPhrase
        // context.setVariable("message.status.code"), ...);
        // context.setVariable("message.reason.phrase"), ...);
        context.setVariable("sage.error.code", response.code);
        context.setVariable("sage.error.info", infoTemplate + response.code);
        context.setVariable("sage.error.detail", JSON.stringify(response.detail));
  //  } else if (typeof response.status == "undefined" && typeof response.message != "undefined") {
     } else if (typeof  response.errorCode != "undefined" && typeof response.errorDescription != "undefined") {
        // REJECTED BY TARGET API. SAMPLE:
        // {
        //   "message": "[\"Invalid Total Amount\"]"
        // }
        //print("REJECTED BY TARGET");
        
        // Next 2 take the code and phrase from TARGET
        // context.setVariable("message.status.code"), ...);
        // context.setVariable(("message.reason.phrase"), ...);

        context.setVariable("sage.error.code", "400000");
        context.setVariable("sage.error.info", infoTemplate + "400000");
        var detail = response.errorCode + " : " + response.errorDescription;
        context.setVariable("sage.error.detail", JSON.stringify(detail));
        
        }
    else
    {
        context.setVariable("sage.error.code", "000000");
        context.setVariable("sage.error.info", infoTemplate + "000000");
        context.setVariable("sage.error.detail", JSON.stringify("Please contact support for assistance."));
    }
} catch (ex) {
    print("CATCH INVALID JSON OR OTHER CONDITION");
    // in case it isn't valid json
    // Let 404 and other back end errors go through, 404 Not Found returns empty body.
    // context.setVariable("sage.error.status", 500);
    // context.setVariable("sage.error.reason", "Unknown Error");
    context.setVariable("sage.error.code", "000000");
    context.setVariable("sage.error.info", infoTemplate + "000000");
    context.setVariable("sage.error.detail", JSON.stringify("Please contact support for assistance."));
}
