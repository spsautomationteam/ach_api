@intg
Feature: Get Credits Details
	As an API consumer
	I want to query credit requests based on the reference
	So that I know they have been processed
	
	@get-Credits-Details_byReference
    Scenario: get Credit details with reference of Sale,Auth and force tranaction
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a credit transaction
        Then response code should be 201
        And response body path $.status should be Approved
        When I Get that same Credit details
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path $.reference should be `codeReference`
		
#	**************** Negative Scenarios **************************	 	
		
	@get-Credits-Detail_byInvalidReference
    Scenario: get Credit details with invalid reference
        Given I have valid credentials
        And I set content-type header to application/json          	
        When I try to get the credit by invalid reference TEST1234
		Then response code should be 404
        And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.

		
#	@get-Credits-Details_tryToGetSaleTransDetails
#    Scenario: query all credits 
#        Given I set clientId header to `clientId`
#        And I set merchantId header to `merchantId`
#        And I set merchantKey header to `merchantKey`
#		And I set content-type header to application/json
#		When I create a sale charge
#        Then response code should be 201
#		When I get that same Sale
#		Then response code should be 201
        

	
				
                                
    