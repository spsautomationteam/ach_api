@intg
Feature: Get Charges
	As an API consumer
	I want to query charge requests
	So that I know they have been processed	  
		
	@get-Charges_getAllTransactions
    Scenario: query all charges from sale transactions
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /charges
        Then response code should be 201
		When I get the list of charges transactions
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
		And response body should contain "paymentType":"ACH"
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should contain Sale
        And response body should not contain Credit
        And response body should not contain transactionCode   

	@get-Charges_byPageNumber
    Scenario: query all charges by PageNumber
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?pageNumber=1
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
		And response body should contain "paymentType":"ACH"
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should contain Sale
        And response body should not contain Credit
        And response body should contain "pageNumber":1		
		
	@get-Charges_byPageSize
    Scenario: query all charges by Page size
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?pageSize=5
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
		And response body should contain "paymentType":"ACH"
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should contain Sale
        And response body should not contain Credit
        And response body should contain "pageSize":5		
		
	@get-Charges_byStartDate
    Scenario: query all charges by StartDate
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?StartDate=2017-04-10
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
		And response body should contain "paymentType":"ACH"
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should contain Sale
        And response body should not contain Credit
        And response body should contain "startDate":"2017-04-10"
		
	@get-Charges_byEndDate
    Scenario: query all charges by End Date
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?EndDate=2017-07-10
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
		And response body should contain "paymentType":"ACH"
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should contain Sale
        And response body should not contain Credit
        And response body should contain "endDate":"2017-07-10"
		
	@get-Charges_bysortField-sortDescending
    Scenario: query all charges by Sorting set to descending
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?sortsortField=Descending&sortField=date
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should contain Sale
        And response body should not contain Credit
        And response body should not contain transactionCode
		
	@get-Charges_bysortField-sortAscending
    Scenario: query all charges by Sorting set to Ascending
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?sortsortField=Ascending&sortField=date
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should contain Sale
        And response body should not contain Credit
        And response body should not contain transactionCode
		
	@get-Charges_byAccountNumber
    Scenario: query all charges by account number
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?accountNumber=1234
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
		And response body should contain "paymentType":"ACH"
        And response body should contain Sale
        And response body should not contain Credit
        And response body should contain "accountNumber":"XXXXXXXXXX1234"		
		
	@get-Charges_byOrderNumber
    Scenario: query all charges by order number
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set content-type header to application/json
		When I perform a Sale Transaction		
        Then response code should be 201
        And response body path $.status should be Approved
		When I use HMAC and GET /charges?orderNumber=`saleOrderNumber`
     #  Then response code should be 200
        And response header Content-Type should be application/json
        And response body path $.orderNumber should be `saleOrderNumber`
		
	@get-Charges_byReference
    Scenario: query all charges by reference
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set content-type header to application/json
		When I perform a Sale Transaction		
        Then response code should be 201
        And response body path $.status should be Approved
		And response body path $.reference should be `saleReference`	
		And response body should contain "message":"ACCEPTED                        "
		When I use HMAC and GET /charges?reference=`saleReference`
        Then response code should be 200
        And response header Content-Type should be application/json		        
		
	@get-Charges_byTotalAmount
    Scenario: query all charges by total amount
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set content-type header to application/json
		When I perform a Sale Transaction		
        Then response code should be 201
        And response body path $.status should be Approved
      # When I use HMAC and GET /charges?totalAmount=`Total`
		When I get that same Sale charge
        Then response code should be 201
        And response header Content-Type should be application/json    
		And response body path $.items[0].amounts.total should be `Total`
		
		
     ######################################Negative scenarios#################################	 
	                              
    @get-Charges_Authorization_NotValid
    Scenario: query all charges from Authorization transactions
         Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /charges?type=Authorization
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain Authorization
        And response body should not contain Credit
        And response body should not contain transactionCode        

    @get-Charges_Force_NotValid
    Scenario: query all charges from force transactions
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /charges?type=Force
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain Force
        And response body should not contain Credit
        And response body should not contain transactionCode  
	
	@get-Charges_byInvalidStartDate
     Scenario: Verify the API Get charges with Future StartDate
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?StartDate=2030-12-12
        Then response code should be 200
        And response body should contain "startDate":"2030-12-12"
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0
		
	@get-Charges_byInvalidEndDate
     Scenario: Verify the API Get Charges with Past EndDate
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?EndDate=2012-12-12
        Then response code should be 200
        And response body should contain "endDate":"2012-12-12"
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0
		
	@get-Charges_byInvalidPageSize
    Scenario: query all charges by InvalidPageSize
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?pageSize=abcdef
        Then response code should be 200
	
	@get-Charges_byInvalidPageNumber
    Scenario: query all charges by InvalidPageNumber
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?pageNumber=abcdef
        Then response code should be 200
		
	@get-Charges_byInvalidSortDirection
    Scenario: query all charges by InvalidSortDirection
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?sortDirection=Testing
        Then response code should be 200
		
	@get-Charges_byInvalidSortField
    Scenario: query all charges by InvalidSortField
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?sortField=TEST
        Then response code should be 400
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be InvalidRequestData : SortField must be a field in the result set
 
	@get-Charges_byInvalidName
    Scenario: query all charges by Invalidname
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?name=qwertsdfgsd
        Then response code should be 200
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0		
		
	@get-Charges_byInvalidAccountNum
    Scenario: query all charges by InvalidAccount
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?accountNumber=qwer
        Then response code should be 200
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0
		
	@get-Charges_byInvalidSource
    Scenario: query all charges by InvalidSource
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?source=qwer
        Then response code should be 200	
		
	@get-Charges_byInvalidOrderNumber
    Scenario: query all charges by InvalidorderNumber
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?orderNumber=qwer
        Then response code should be 200
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0
		
	@get-Charges_byInvalidReference
    Scenario: query all charges by InvalidReference
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?reference=qwer
        Then response code should be 200
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0			
			
	@get-Charges_byInvalidBatchRef
    Scenario: query all charges by InvalidBatchRef
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?batchReference=qwer
        Then response code should be 200
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0		
			
	@get-Charges_byInvalidTotalAmount
    Scenario: query all charges by InvalidTotalAmount
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /charges?totalAmount=9999
        Then response code should be 200
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0
         
                

	