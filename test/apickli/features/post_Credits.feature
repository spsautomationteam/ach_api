@Post_Credits
Feature: Credits Create
	As an API consumer
	I want meaningful responses to credit requests
	So that I know they where process correctly

	@post-credit_byAllTranClassType
    Scenario Outline: Post a credit for different Transaction class and account type
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
		And I set body to { "transactionClass": <TxClass>, "amounts":{ "total": 1.0 }, "account":{ "type": <accountType>, "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /credits
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path $.status should be Approved
		And response body path $.message should be ACCEPTED
	Examples:
	|TxClass|accountType|
	|"ARC"|"Savings"|
	|"ARC"|"Checking"|
	|"CCD"|"Savings"|
	|"CCD"|"Checking"|
	|"PPD"|"Savings"|
	|"PPD"|"Checking"|
	|"RCK"|"Savings"|
	|"RCK"|"Checking"|
	|"TEL"|"Savings"|
	|"TEL"|"Checking"|
	|"WEB"|"Savings"|
	|"WEB"|"Checking"|
	
	@post-credit-withAlltagData
    Scenario: Post a credit with all tags data passed [Except Prior Reference Tag]
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
		And I set body to {     "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "001111111111",         "fax": "00111111"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {         "token": "2a9e926e052b405bb3c71565f4d2d616",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
		When I use HMAC and POST to /credits
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path $.status should be Approved
		And response body path $.message should be ACCEPTED
		
	@post-credit-withVaultCreate
    Scenario: Post a credit with vault operation as Create
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" },"vault": {  "operation": "Create"     }}
		When I use HMAC and POST to /credits
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path $.status should be Approved
		And response body path $.message should be ACCEPTED
		And response body path $.vaultResponse.message should be Success
		
	@post-credit_withVaultRead
    Scenario: Post a credit with vault operation as Read
		Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
		When I create a vault token with body set to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 200
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" },"vault": {         "token": "`token`",         "operation": "Read"     }}
		When I use HMAC and POST to /credits
		Then response code should be 201
        And response header Content-Type should be application/json
        And response body path $.status should be Approved
		And response body path $.message should be ACCEPTED
		And response body should not contain vaultResponse	
		
	@post-credit-withVaultUpdate
    Scenario: Post a credit with vault operation as Update
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
		When I create a vault token with body set to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 200
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" },"vault": {         "token": "`token`",         "operation": "Update"     }}
		When I use HMAC and POST to /credits
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path $.status should be Approved
		And response body path $.message should be ACCEPTED
		And response body path $.vaultResponse.message should be Success
		
		
		#############  Negative Scenarios ####################
		
			
	@post-Credits-withoutTransClassTag
    Scenario: Post a credit without transactionClass tag
		Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
		And I set body to {  "amounts":{"total": 1.0  }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /credits
        Then response code should be 400
		And response body path $.detail should be InvalidRequestData : request: SecCode is required
		
	@post-Credits-byBlankTransClassTag
    Scenario: Post a credit with empty transactionClass
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /credits
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body path $.detail should be InvalidRequestData : request: SecCode is required

    @post-Credits-byInvalid-TransClass
    Scenario: Post a credit with invalid transactionClass
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "ABC", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /credits
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain "InvalidRequestData : request: SecCode is required; Error converting value
		
	@post-Credits-byNegativeAmount
    Scenario: Post a credit for invalidAmount
		Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
		And I set body to { "transactionClass": "PPD", "amounts":{"total": -31.0  }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /credits
        Then response code should be 400
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be InvalidRequestData : request.Amounts: Invalid Total Amount
		
	@post-Credits-byinvalidAmount
    Scenario: Post a credit with invalid amount
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": "ABC" }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /credits
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain Could not convert string to decimal.
		
	@post-Credits-missingAmounts
    Scenario: Post a credit with missing amounts
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /credits
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain The Amounts field is required.
				
	@post-Credits-byBlankTotalAmount
    Scenario: Post a credit without total amount value
		Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
		And I set body to { "transactionClass": "PPD", "amounts":{  }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /credits
        Then response code should be 400
		And response body path $.detail should be InvalidRequestData : request: Total is required		
	
	@post-Credits-emptyAccount
    Scenario: Post a credit with empty account
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{  }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /credits
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain The Type field is required.
        And response body should contain The RoutingNumber field is required.
        And response body should contain The AccountNumber field is required.
		
	@post-Credits-invalidAccountNo
    Scenario: Post a credit for invalid AccountNumber
		Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "123" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /credits
        Then response code should be 401
		And response body path $.detail should be You are a using certification environment which is restricted to test data only.
		
	@post-Credits-withoutAccountNo
    Scenario: Post a credit without AccountNumber
		Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /credits
        Then response code should be 400
		And response body path $.detail should be InvalidRequestData : request.Account: The AccountNumber field is required.
		
	@post-Credits-invalidRoutingNo
    Scenario: Post a credit for invalid Routing Number
		Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "123", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /credits
        Then response code should be 401
		And response body path $.detail should be You are a using certification environment which is restricted to test data only.
		
	@post-Credits-withoutRoutingNo
    Scenario: Post a credit without RoutingNumber
		Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking","accountNumber": "12345678901234"  }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /credits
        Then response code should be 400
		And response body path $.detail should be InvalidRequestData : request.Account: The RoutingNumber field is required.
		
	@post-Credits-WithoutCustomerData_TxClassCCD
    Scenario: Post a credit for CCD Tx class
		Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
		And I set body to { "transactionClass": "CCD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /credits
        Then response code should be 400
		And response body path $.detail should be InvalidRequestData : Customer is required		
		
	@post-Credits-WithoutCustomerData_TxClassWEB
    Scenario: Post a credit for CCD Tx class
		Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
		And I set body to { "transactionClass": "WEB", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /credits
        Then response code should be 400
		And response body path $.detail should be InvalidRequestData : request: Customer is required
		
	@post-Credits-withoutAccountType
    Scenario: Post a credit for invalidAccountNumber
		Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		When I use HMAC and POST to /credits
        Then response code should be 400
		And response body path $.detail should be InvalidRequestData : request.Account: The Type field is required.
		
	@post-Credits-emptyBilling
    Scenario: Post a credit with empty billing
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": {  } }
        When I use HMAC and POST to /credits
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain The Name field is required.

    @post-Credits-emptyName
    Scenario: Post a credit with empty name
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": {} } }
        When I use HMAC and POST to /credits
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain The First field is required
        And response body should contain The Last field is required

    @post-Credits-emptyNames
    Scenario: Post a credit with empty names
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "First": "", "Last": "" } } }
        When I use HMAC and POST to /credits
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain The First field is required
        And response body should contain The Last field is required

    @post-Credits-missingAddresses
    Scenario: Post a credit with missing address data
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "First": "foo", "Last": "bar" } } }
        When I use HMAC and POST to /credits
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain Address is required
        And response body should contain City is required
        And response body should contain State is required
        And response body should contain PostalCode is required

    @post-Credits-emptyAddresses
    Scenario: Post a credit with empty address data
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "foo", "last": "bar" }, "address": "", "city": "", "state": "", "postalCode": "" } }
        When I use HMAC and POST to /credits
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain Address is required
        And response body should contain City is required
        And response body should contain State is required
        And response body should contain PostalCode is required

        
