@intg
Feature: Get Charges Detail
	As an API consumer
	I want to query charge requests based on the reference
	So that I know they have been processed

	@get-Charges-Detials_byReference_Sale
    Scenario: query charge details from reference of Sale,Auth and force tranaction
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a Sale charge
        Then response code should be 201
        And response body path $.status should be Approved
	#	When I use HMAC and GET /charges/`saleReference`
        When I get that same Sale charge
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path $.reference should be `saleReference`  
		
		
		####################### Negative Scenarios ########################
		
	@get-Charges-Detials_byReference_Force
    Scenario: query charge details from reference of Force tranaction
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a Force charge
        Then response code should be 201
        And response body path $.status should be Approved
        When I get that same Force charge
        Then response code should be 404
        And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.
		
	@get-Charges-Detials_byReference_Auth
    Scenario: query charge details from reference of Authorization tranaction
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a Authorization charge
        Then response code should be 201
        And response body path $.status should be Approved
        When I get that same Authorization charge
        Then response code should be 404
        And response body path $.code should be 000000
		And response body path $.message should be Missing or invalid Application Identifier
		And response body path $.detail should be A valid Application ID is required in header parameter clientId.
	
	@get-Charge-Details_byInvalidReference
    Scenario: query charge details from invalid reference
        Given I have valid credentials
        And I set content-type header to application/json        
     	When I try to get the charge by invalid reference ABCABCABC	
	#	When I use HMAC and GET /charges/ABCABCABC
		Then response code should be 404
		And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.
        

	
				
                                
    