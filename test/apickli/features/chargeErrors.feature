@intg
@Charge_Errors
Feature: Charges back end error conditions
    As an API consumer
    I want to create invalid charge requests
    So that I know error conditions are working

    @post-Sale_byMissingAccount
    Scenario: Post a sale with missing Account
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain InvalidRequestData : request: Account is required

    @post-Sale_byEmptyAccount
    Scenario: Post a sale with empty account
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{  }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain The Type field is required.
        And response body should contain The RoutingNumber field is required.
        And response body should contain The AccountNumber field is required.

    @post-Sale_byMissingRouting
    Scenario: Post a sale with missing routing
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain The RoutingNumber field is required.

    @post-Sale_byEmptyRouting
    Scenario: Post a sale with empty routing
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain The RoutingNumber field is required.

    @post-Sale_byMissingAccount
    Scenario: Post a sale with missing account
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain The AccountNumber field is required.

    @post-Sale_byEmptyAccount
    Scenario: Post a sale with empty account
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain The AccountNumber field is required.

    @post-Sale_byMissingType
    Scenario: Post a sale with missing type
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain The Type field is required.

    @post-Sale_byEmptyType
    Scenario: Post a sale with empty type
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain The Type field is required.

    @post-Sale_byMissingTrxClass
    Scenario: Post a sale with missing transactionClass
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain SecCode is required

    @post-Sale_byEmptyTrxClass
    Scenario: Post a sale with empty transactionClass
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain SecCode is required

    @post-Sale_invalidTrxClass
    Scenario: Post a sale with invalid transactionClass
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "ABC", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain Error converting value

    @post-Sale_byMissingBilling
    Scenario: Post a sale with missing billing
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain The Billing field is required.

    @post-Sale_byEmptyBilling
    Scenario: Post a sale with empty billing
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": {  } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain The Name field is required.

    @post-Sale_byEmptyName
    Scenario: Post a sale with empty name
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": {} } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain The First field is required
        And response body should contain The Last field is required

    @post-Sale_byEmptyNames
    Scenario: Post a sale with empty names
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "First": "", "Last": "" } } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain The First field is required
        And response body should contain The Last field is required

    @post-Sale_byMissingAddresse
    Scenario: Post a sale with missing address data
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "First": "foo", "Last": "bar" } } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain Address is required
        And response body should contain City is required
        And response body should contain State is required
        And response body should contain PostalCode is required

    @post-Sale_byEmptyAddress
    Scenario: Post a sale with empty address data
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "foo", "last": "bar" }, "address": "", "city": "", "state": "", "postalCode": "" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain Address is required
        And response body should contain City is required
        And response body should contain State is required
        And response body should contain PostalCode is required

    @post-Sale_byMissingAmount
    Scenario: Post a sale with missing amounts
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain The Amounts field is required.

    @post-Sale_invalidAmount
    Scenario: Post a sale with invalid amount
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to { "transactionClass": "PPD", "amounts":{ "total": "ABC" }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 400000
        And response body should contain Could not convert string to decimal.
