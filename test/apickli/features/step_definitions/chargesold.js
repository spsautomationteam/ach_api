/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var hmacTools = new (require("../../../hmacTools.js"))();

const runSale = (apickli, callback) => {
    var pathSuffix = "/charges";
    var url = apickli.domain + pathSuffix;
    var body = '{ "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }'
    
    apickli.setRequestBody(body);
    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
    apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
    apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
    apickli.addRequestHeader('nonce', nonce);
    apickli.addRequestHeader('timestamp', timestamp);
    apickli.addRequestHeader('Authorization', hmac);

    apickli.post(pathSuffix, (err, response) =>  {
        apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'saleReference');
		console.log("Trans RefNumber : "+ apickli.scenarioVariables.saleReference)	
		var Reference = apickli.scenarioVariables.saleReference;
		
		apickli.storeValueOfResponseBodyPathInScenarioScope('$.orderNumber', 'SaleorderNumber');
		console.log("Trans orderNumber : "+ apickli.scenarioVariables.SaleorderNumber)	
		var Reference = apickli.scenarioVariables.SaleorderNumber;
		
        apickli.setRequestBody("");
        callback();
    });
};

const getCharge = (apickli, saleReference, callback) => {
    var pathSuffix = "/charges/" + saleReference;
    var url = apickli.domain + pathSuffix;

    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.headers['nonce'] = nonce;
    apickli.headers['timestamp'] = timestamp;
    apickli.headers['Authorization'] = hmac;

    apickli.get(pathSuffix, callback);
};

module.exports = function () { 
    this.Given(/^I have a valid credit card and valid amount$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
	});
    this.Given(/^I have a valid credit card number and invalid amount$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
        });
    this.Given(/^I have an invalid credit card number and valid amount$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
	});
    this.Given(/^I have an invalid credit card expiration date and valid amount$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
	});
    this.Given(/^I have an invalid credit card security code and valid amount$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
	});
    this.Given(/^I perform a Sale Transaction$/, function (callback) {
        runSale(this.apickli, callback);
    });
    this.When(/^I get that same charge$/, function (callback) {
        var saleReference = this.apickli.scenarioVariables.saleReference;
        // console.log("REFERENCE: ", authorizationReference );
        getCharge(this.apickli, saleReference, callback);
    });
};
