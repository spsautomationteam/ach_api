/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var hmacTools = new (require("../../../hmacTools.js"))();

const runSale = (apickli,typeOfTx, callback) => {
 	
    var pathSuffix = "/charges?type="+typeOfTx;
	
	var totAmount =Math.floor(Math.random() * 1000);
	
    var url = apickli.domain + pathSuffix;
	
	var body = '{ "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }'
    
    apickli.setRequestBody(body);
    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
    apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
    apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
    apickli.addRequestHeader('nonce', nonce);
    apickli.addRequestHeader('timestamp', timestamp);
    apickli.addRequestHeader('Authorization', hmac);

    apickli.post(pathSuffix, (err, response) =>  {
		console.log(response.body);
        apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'saleReference');
		apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'creditReference');
		apickli.storeValueOfResponseBodyPathInScenarioScope('$.orderNumber', 'saleOrderNumber');
		apickli.storeValueOfResponseBodyPathInScenarioScope('$.code', 'saleCode');
		apickli.storeValueOfResponseBodyPathInScenarioScope('$.total', 'Total');
        apickli.setRequestBody("");
        callback();
    });
};

const runCredit = (apickli, callback) => {
 	
    var pathSuffix = "/credits";
	
	var totAmount =Math.floor(Math.random() * 1000);
	
    var url = apickli.domain + pathSuffix;
	
	var body = '{ "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "midhun@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }'
    
    apickli.setRequestBody(body);
    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
    apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
    apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
    apickli.addRequestHeader('nonce', nonce);
    apickli.addRequestHeader('timestamp', timestamp);
    apickli.addRequestHeader('Authorization', hmac);

    apickli.post(pathSuffix, (err, response) =>  {
		console.log(response.body);
        apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'saleReference');
		apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'creditReference');
		apickli.storeValueOfResponseBodyPathInScenarioScope('$.orderNumber', 'saleOrderNumber');
		apickli.storeValueOfResponseBodyPathInScenarioScope('$.code', 'saleCode');
		apickli.storeValueOfResponseBodyPathInScenarioScope('$.total', 'Total');
        apickli.setRequestBody("");
        callback();
    });
};

const getCharge = (apickli, saleReference, callback) => {
    var pathSuffix = "/charges/" + saleReference;
		var url = apickli.domain + pathSuffix;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac(clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;

		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};


const getChargeInvRef = (apickli, saleReference, callback) => {
    var pathSuffix = "/charges/" + saleReference;
    var url = apickli.domain + pathSuffix;
	console.log(" URL>>>>>>>>>>>>>> :"+url);
    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
	apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
    apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
    apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
    apickli.addRequestHeader('nonce', nonce);
    apickli.addRequestHeader('timestamp', timestamp);
    apickli.addRequestHeader('Authorization', hmac);	

    apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};


var getChargesLists = function( apickli, callback ) {
		var pathSuffix = "/charges";
		var url = apickli.domain + pathSuffix;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac(clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;

		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

const deleteCharge = (apickli, saleReference, callback) => {
		var pathSuffix = "/charges/" + saleReference;
		console.log("pathSuffix : "+pathSuffix);
		var url = apickli.domain + pathSuffix;

		var nonce = hmacTools.nonce(12);
		var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
	    var hmac = hmacTools.hmac(clientSecret, "DELETE", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
	    apickli.headers['Authorization'] = hmac;
		
		
		apickli.delete(pathSuffix, function (err, response) {
			if ( response ) {
			 console.log( response.body);
			}
        	callback();
		});
};

const deleteChargeInvRef = (apickli, saleReference, callback) => {
		var pathSuffix = "/charges/" + saleReference;
		console.log("pathSuffix : "+pathSuffix);
		var url = apickli.domain + pathSuffix;

		var nonce = hmacTools.nonce(12);
		var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
	    var hmac = hmacTools.hmac(clientSecret, "DELETE", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		
	apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
    apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
    apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
    apickli.addRequestHeader('nonce', nonce);
    apickli.addRequestHeader('timestamp', timestamp);
    apickli.addRequestHeader('Authorization', hmac);
		
		apickli.delete(pathSuffix, function (err, response) {
			if ( response ) {
			 console.log( response.body);
			}
        	callback();
		});
};

module.exports = function () { 
    this.Given(/^I have a valid credit card and valid amount$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
	});
    this.Given(/^I have a valid credit card number and invalid amount$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
        });
    this.Given(/^I have an invalid credit card number and valid amount$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
	});
    this.Given(/^I have an invalid credit card expiration date and valid amount$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
	});
    this.Given(/^I have an invalid credit card security code and valid amount$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
	});
	
    this.When(/^I create a (.*) charge$/, function (typeOfTx, callback) {       
        runSale(this.apickli, typeOfTx, callback);
    });	
	
	 this.When(/^I perform a credit charge$/, function ( callback) {       
        runCredit(this.apickli, callback);
    });
	
    this.When(/^I get that same (.*) charge$/, function (callback) {
        var saleReference = this.apickli.scenarioVariables.saleReference;        
        getCharge(this.apickli, saleReference, callback);
    });	
	
	this.When(/^I get the list of charges transactions$/, function ( callback) {
      	//this.apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');
		getChargesLists(this.apickli, callback);

	});
	
	this.When(/^I try to get the charge by invalid reference (.*)$/, function (invalidRef,callback) {
        var saleReference = invalidRef;       
        getChargeInvRef(this.apickli, saleReference, callback);
    });
	
	this.When(/^I delete that same charge$/, function (callback) {
        var saleReference = this.apickli.scenarioVariables.saleReference;       
        deleteCharge(this.apickli, saleReference, callback);
    });
	
	this.When(/^I delete the charge with invalid reference (.*)$/, function (invalidRef, callback) {
        var saleReference = invalidRef;       
        deleteChargeInvRef(this.apickli, saleReference, callback);
    });
	
};
