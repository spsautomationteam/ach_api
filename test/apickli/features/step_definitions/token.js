/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var hmacTools = new (require("../../../hmacTools.js"))();

const createToken = (apickli,jsonBody, callback) => {
    var pathSuffix = "/tokens";
    var url = apickli.domain + pathSuffix;
    var body = jsonBody;
    
    apickli.setRequestBody(body);
    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
    apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
    apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);	
    apickli.addRequestHeader('nonce', nonce);
    apickli.addRequestHeader('timestamp', timestamp);
    apickli.addRequestHeader('Authorization', hmac);
	apickli.addRequestHeader('Content-Type', 'application/json');

    apickli.post(pathSuffix, (err, response) =>  {
		console.log(response.body);
        apickli.storeValueOfResponseBodyPathInScenarioScope('$.vaultResponse.data', 'token');
		console.log("Trans Token : "+ apickli.scenarioVariables.token);
		
		apickli.setRequestBody("");		
        callback();
    });
};

const updateToken = (apickli, Reference,jsonBody, callback) => {
    var pathSuffix = "/tokens/" + Reference;
  
    var url = apickli.domain + pathSuffix;
    var body = jsonBody ;
     
    apickli.setRequestBody(body);
    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "PUT", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.headers['nonce'] = nonce;
    apickli.headers['timestamp'] = timestamp;
    apickli.headers['Authorization'] = hmac;

    apickli.put(pathSuffix, (err, response) =>  {
		console.log(response.body);        		
        callback();
    });
};

const deleteToken = (apickli, Reference, callback) => {
    var pathSuffix = "/tokens/" + Reference;
    var url = apickli.domain + pathSuffix;

    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "DELETE", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.headers['nonce'] = nonce;
    apickli.headers['timestamp'] = timestamp;
    apickli.headers['Authorization'] = hmac;

    apickli.delete(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

const deleteTokenByInvRef = (apickli, Reference, callback) => {
    var pathSuffix = "/tokens/" + Reference;
    var url = apickli.domain + pathSuffix;

    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

   var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "DELETE", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
    apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
    apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);	
    apickli.addRequestHeader('nonce', nonce);
    apickli.addRequestHeader('timestamp', timestamp);
    apickli.addRequestHeader('Authorization', hmac);
	apickli.addRequestHeader('Content-Type', 'application/json');

    apickli.delete(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};


module.exports = function () { 
    
    this.Given(/^I create a vault token with body set to (.*)$/, function (jsonBody,callback) {
        createToken(this.apickli, jsonBody,callback);
    });
    
	this.When(/^I update card data for the above token with body set to (.*)$/, function (jsonBody,callback) {
        var Reference = this.apickli.scenarioVariables.token;	
		updateToken(this.apickli, Reference,jsonBody, callback);
		
    });
	
	this.When(/^I try to update card data by invalid token reference (.*) with body set to (.*)$/, function (invalidRef,jsonBody,callback) {
        var Reference = invalidRef;        
        updateToken(this.apickli, Reference,jsonBody, callback);
    });
	 this.When(/^I delete that same token$/, function (callback) {
        var Reference = this.apickli.scenarioVariables.token;
        
        deleteToken(this.apickli, Reference, callback);
    });
	
	this.When(/^I delete the token with invalid reference (.*)$/, function (invalidRef,callback) {
        var Reference = invalidRef;       
        deleteTokenByInvRef(this.apickli, Reference, callback);
    });
};
