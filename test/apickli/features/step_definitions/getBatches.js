/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var config = require('../../config/config.json');var hmacTools = new (require("../../../hmacTools.js"))();
var Contenttype = "application/json";

var createSale = function( apickli, callback ) {
		var pathSuffix = "/charges?type=Sale";
		var url = apickli.domain + pathSuffix;
		
		var totAmount =Math.floor(Math.random() * 1000);
		
	//	var body = { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		var body = { "transactionClass": "PPD", "amounts":{ "total": totAmount }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		
		var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
        apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
        apickli.addRequestHeader('nonce', nonce);
        apickli.addRequestHeader('timestamp', timestamp);
		apickli.addRequestHeader('Content-Type', Contenttype);
        apickli.addRequestHeader('Authorization', hmac);

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};


var batchSettle = function(apickli,callback ) {
		var pathSuffix = "/batches/current";
		var url = apickli.domain + pathSuffix;
		var body = {"settlementType": "ACH"};
		var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['clientId'] = apickli.scenarioVariables.clientId;
		apickli.headers['merchantId'] = apickli.scenarioVariables.merchantId;
		apickli.headers['merchantKey'] = apickli.scenarioVariables.merchantKey;
        apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.headers['Authorization'] = hmac;

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
				apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'batchReference');

			}
        	callback();
		});
		
};		
		
const getBatch = (apickli, callback) => {
    var pathSuffix = "/batches";
    var url = apickli.domain + pathSuffix;

    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.headers['nonce'] = nonce;
    apickli.headers['timestamp'] = timestamp;
    apickli.headers['Authorization'] = hmac;

   // apickli.get(pathSuffix, callback);
	apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);				
			}
			  apickli.storeValueOfResponseBodyPathInScenarioScope('$.items[0].reference', 'batchReference');		
				  console.log("Batch RefNumber : "+ apickli.scenarioVariables.batchReference);
				  apickli.storeValueOfResponseBodyPathInScenarioScope('$.items[0].count', 'TransCount');		
				  console.log("Batch TransCount : "+ apickli.scenarioVariables.TransCount);
				  apickli.storeValueOfResponseBodyPathInScenarioScope('$.items[0].net', 'TransNet');		
				  console.log("Batch TransNet : "+ apickli.scenarioVariables.TransNet);
			
        	callback();
		});
};

const getBatchByReference = (apickli, BatchReference, callback) => {
    var pathSuffix = "/batches/"+BatchReference;
    var url = apickli.domain + pathSuffix;

    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.headers['nonce'] = nonce;
    apickli.headers['timestamp'] = timestamp;
    apickli.headers['Authorization'] = hmac;

   // apickli.get(pathSuffix, callback);
	apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);				
			}
			  apickli.storeValueOfResponseBodyPathInScenarioScope('$.items[1].reference', 'batchReference');		
				  console.log("Batch RefNumber : "+ apickli.scenarioVariables.batchReference);
				 apickli.storeValueOfResponseBodyPathInScenarioScope('$.totalItemCount', 'TranstotalItemCount');		
				  console.log("Batch Trans TranstotalItemCount : "+ apickli.scenarioVariables.TranstotalItemCount);
				  apickli.storeValueOfResponseBodyPathInScenarioScope('$.volume', 'TransVolume');		
				  console.log("Batch TransVolume : "+ apickli.scenarioVariables.TransVolume);
				  
        	callback();
		});
};

const getBatchSummaryByReference = (apickli, BatchReference, callback) => {
    var pathSuffix = "/batches/"+BatchReference+"/summary";
    var url = apickli.domain + pathSuffix;

    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.headers['nonce'] = nonce;
    apickli.headers['timestamp'] = timestamp;
    apickli.headers['Authorization'] = hmac;

	apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);				
			}			  
				 apickli.storeValueOfResponseBodyPathInScenarioScope('$.totalCount', 'TotalSummaryCount');		
				  console.log("Batch Summary TotalCount : "+ apickli.scenarioVariables.TotalCount);
				  apickli.storeValueOfResponseBodyPathInScenarioScope('$.totalVolume', 'TotalSummaryVolume');		
				  console.log("Batch Summary TotalVolume : "+ apickli.scenarioVariables.TotalVolume);
				  
        	callback();
		});
};


module.exports = function () { 

	this.Given(/^I create a sale transaction$/, function (callback) {
		createSale(this.apickli, callback);
	});
	
	this.When(/^I settle all current set of transaction$/, function (callback) {
       
		batchSettle(this.apickli,callback);
	});
	
	this.When(/^I get all Batch Transaction Details$/, function (callback) {        
        getBatch(this.apickli, callback);
    });	
	
	this.When(/^I get Batche Details With Reference Number$/, function (callback) {
        var batchReference = this.apickli.scenarioVariables.batchReference;
          console.log("Batch RefNumber : ", batchReference );
        getBatchByReference(this.apickli, batchReference, callback);
    });
	
	this.Then(/^Verify Get Batches 'Count' and 'Volumes' values Matching at Get Batches Reference$/, function (callback) {
		
		var GetBatchRef = this.apickli.scenarioVariables.batchReference;
		var BatchTransCount = this.apickli.scenarioVariables.TransCount;
		var BatchTransNet = this.apickli.scenarioVariables.TransNet;

		var GetBatchRefNumber = this.apickli.scenarioVariables.batchReference;
		var BatchTranstotalItemCount = this.apickli.scenarioVariables.TranstotalItemCount;
		var BatchTransVolume = this.apickli.scenarioVariables.TransVolume;
		if(GetBatchRef == GetBatchRefNumber)
			if(BatchTransCount == BatchTranstotalItemCount)
				if(BatchTransNet == BatchTransVolume)
					console.log("'Count' and 'Volumes' values Matching at Get Batches and 'Get Batches Reference' ");
         callback();
    });
	
	this.When(/^I get Batche Summary Details With GetBatch Reference Number$/, function (callback) {
        var batchReference = this.apickli.scenarioVariables.batchReference;
          console.log("Batch RefNumber : ", batchReference );
        getBatchSummaryByReference(this.apickli, batchReference, callback);
    });
	
	
	this.Then(/^Verify Get Batches 'Count' and 'Volumes' values Matching at Get Batches Summary Reference API$/, function (callback) {
		
		var BatchTransCount = this.apickli.scenarioVariables.TransCount;
		var BatchTransNet = this.apickli.scenarioVariables.TransNet;

		var BatchSummaryItemCount = this.apickli.scenarioVariables.TotalSummaryCount;
		var BatchSummaryTransVolume = this.apickli.scenarioVariables.TotalSummaryVolume;
		
			if(BatchTransCount == BatchSummaryItemCount)
				if(BatchTransNet == BatchSummaryTransVolume)
					console.log("'Count' and 'Volumes' values Matching at Get Batches and 'Get Batches Summary Reference' ");
         callback();
    });

	};
