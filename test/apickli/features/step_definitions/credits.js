/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var hmacTools = new (require("../../../hmacTools.js"))();

const runCredit = (apickli, callback) => {
    var pathSuffix = "/credits";
	console.log(" Credit pathSuffix : "+pathSuffix)
    var url = apickli.domain + pathSuffix;
	
	var totAmount =Math.floor(Math.random() * 1000);
	
	var body = '{ "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Savings", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }'

    apickli.setRequestBody(body);
    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
    apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
    apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
    apickli.addRequestHeader('nonce', nonce);
    apickli.addRequestHeader('timestamp', timestamp);
    apickli.addRequestHeader('Authorization', hmac);

    apickli.post(pathSuffix, (err, response) =>  {
		console.log(response.body);
        apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'creditReference');
		apickli.storeValueOfResponseBodyPathInScenarioScope('$.orderNumber', 'creditOrderNumber');
		apickli.storeValueOfResponseBodyPathInScenarioScope('$.code', 'creditCode');
		// apickli.storeValueOfResponseBodyPathInScenarioScope('$.amounts.total', 'creditAmount');
		// console.log("Credit Trans Amount : "+ apickli.scenarioVariables.creditAmount)

        apickli.setRequestBody("");		
        callback();
    });
};

const getCredit = (apickli, creditReference, callback) => {
  //  var pathSuffix = "/credits/"+creditReference;
	var pathSuffix = "/credits?Reference="+creditReference;
    var url = apickli.domain + pathSuffix;

    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
	//	apickli.headers['Content-Type'] = Contenttype;
		
		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

const getCreditDetailsInvRef = (apickli, saleReference, callback) => {
    var pathSuffix = "/credits/" + saleReference;
    var url = apickli.domain + pathSuffix;

    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
	apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
    apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
    apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
    apickli.addRequestHeader('nonce', nonce);
    apickli.addRequestHeader('timestamp', timestamp);
    apickli.addRequestHeader('Authorization', hmac);	

    apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

const getCreditDetails = (apickli, creditReference, callback) => {
	 var pathSuffix = "/credits/"+creditReference;
    var url = apickli.domain + pathSuffix;

    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
	//	apickli.headers['Content-Type'] = Contenttype;
		
		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

var getCreditLists = function( apickli, callback ) {
		var pathSuffix = "/credits";
		var url = apickli.domain + pathSuffix;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac(clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;

		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

const deleteCredit = (apickli, creditReference, callback) => {
    var pathSuffix = "/credits/" + creditReference;
    var url = apickli.domain + pathSuffix;

    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "DELETE", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.headers['nonce'] = nonce;
    apickli.headers['timestamp'] = timestamp;
    apickli.headers['Authorization'] = hmac;

    apickli.delete(pathSuffix, function (err, response) {
			if ( response ) {
				//console.log( response.body);
			}
        	callback();
		});
};

const deleteCreditByInvRef = (apickli, creditReference, callback) => {
    var pathSuffix = "/credits/" + creditReference;
    var url = apickli.domain + pathSuffix;

    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "DELETE", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;

    apickli.delete(pathSuffix, function (err, response) {
			if ( response ) {
				//console.log( response.body);
			}
        	callback();
		});
};

const postCreditRef = (apickli, Reference,jsonBody, callback) => {
    var pathSuffix = "/credits/" + Reference;
    
    var url = apickli.domain + pathSuffix;
    var body = jsonBody ;
    console.log(url);
    apickli.setRequestBody(body);
    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.headers['nonce'] = nonce;
    apickli.headers['timestamp'] = timestamp;
    apickli.headers['Authorization'] = hmac;

    apickli.post(pathSuffix, (err, response) =>  {
		console.log(response.body);        		
        callback();
    });
};

module.exports = function () { 
    this.Given(/^I have a valid credit card and valid amount$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
                });
    this.Given(/^I have a valid credit card number and invalid amount$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
        });
    this.Given(/^I have an invalid credit card number and valid amount$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
                });
    this.Given(/^I have an invalid credit card expiration date and valid amount$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
                });
    this.Given(/^I have an invalid credit card security code and valid amount$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
                });
    this.Given(/^I create a credit transaction$/, function (callback) {
        runCredit(this.apickli, callback);
    });

	// I get that same credit
    this.When(/^I Get that same (.*)$/, function (callback) {
        var creditReference = this.apickli.scenarioVariables.creditReference;       
        getCreditDetails(this.apickli, creditReference, callback);
    });
	
	this.When(/^I get the list of credit transactions$/, function ( callback) {
      	//this.apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');
		getCreditLists(this.apickli, callback);

	});
	
	this.When(/^I try to get the credit by invalid reference (.*)$/, function (invalidRef, callback) {
       // var creditReference = invalidRef;        
        getCreditDetailsInvRef(this.apickli, invalidRef, callback);
    });
	
	this.When(/^I delete that same credit$/, function (callback) {
        var creditReference = this.apickli.scenarioVariables.creditReference;
        
        deleteCredit(this.apickli, creditReference, callback);
    });
	
	this.When(/^I delete the credit with invalid reference (.*)$/, function (invalidRef,callback) {
        var creditReference = invalidRef;       
        deleteCreditByInvRef(this.apickli, creditReference, callback);
    });
	
	this.When(/^I perform post credit reference on the same (.*) transaction with body set to (.*)$/, function (typeOfTx,jsonBody,callback) {
        if(typeOfTx == "Sale") 	
		{
			var Reference = this.apickli.scenarioVariables.saleReference;
		}	
			
		else
		{
			var Reference = this.apickli.scenarioVariables.creditReference;
		}
		postCreditRef(this.apickli, Reference,jsonBody, callback);
    }); 
	
	this.When(/^I perform post credit reference with invalid reference (.*) with body set to (.*)$/, function (invalidRef,jsonBody,callback) {
        var Reference = invalidRef;        
        postCreditRef(this.apickli, Reference,jsonBody, callback);
    });
};
