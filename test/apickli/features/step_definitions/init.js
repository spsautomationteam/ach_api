/* jshint node:true */
'use strict';

var factory = require('./factory.js');
var config = require('../../config/config.json');

var invalidClientId = config.ach.invalidClientId;
var clientId = config.ach.clientId;
var clientSecret = config.ach.clientSecret;
var merchantId = config.ach.merchantId;
var merchantKey = config.ach.merchantKey;
var basepath = config.ach.basepath;
var apiproxy = config.ach.apiproxy;

module.exports = function() {
    // cleanup before every scenario
    this.Before(function(scenario, callback) {
		console.log(config);
        this.apickli = factory.getNewApickliInstance();
        this.apickli.storeValueInScenarioScope("invalidClientId", invalidClientId);
        this.apickli.storeValueInScenarioScope("clientId", clientId);
        this.apickli.storeValueInScenarioScope("clientSecret", clientSecret);
		this.apickli.storeValueInScenarioScope("merchantId", merchantId);
		this.apickli.storeValueInScenarioScope("merchantKey", merchantKey);
		this.apickli.storeValueInScenarioScope("basepath", basepath);
        this.apickli.storeValueInScenarioScope("apiproxy", apiproxy);
        callback();
    });
};

