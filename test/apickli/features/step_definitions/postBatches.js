/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var config = require('../../config/config.json');var hmacTools = new (require("../../../hmacTools.js"))();
var Contenttype = "application/json";

var createSale = function( apickli, callback ) {
		var pathSuffix = "/charges?type=Sale";
		var url = apickli.domain + pathSuffix;
		
		var totAmount =Math.floor(Math.random() * 1000);
		
	//	var body = { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		var body = { "transactionClass": "PPD", "amounts":{ "total": totAmount }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
		
		var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
        apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
        apickli.addRequestHeader('nonce', nonce);
        apickli.addRequestHeader('timestamp', timestamp);
		apickli.addRequestHeader('Content-Type', Contenttype);
        apickli.addRequestHeader('Authorization', hmac);

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};


var batchSettle = function(apickli,callback ) {
		var pathSuffix = "/batches/current";
		var url = apickli.domain + pathSuffix;
		var body = {"settlementType": "ACH"};
		var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['clientId'] = apickli.scenarioVariables.clientId;
		apickli.headers['merchantId'] = apickli.scenarioVariables.merchantId;
		apickli.headers['merchantKey'] = apickli.scenarioVariables.merchantKey;
        apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.headers['Authorization'] = hmac;

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
				apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'batchReference');

			}
        	callback();
		});
		
};		
		
const getBatch = (apickli, batchReference, callback) => {
    var pathSuffix = "/batches";
    var url = apickli.domain + pathSuffix;

    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.headers['nonce'] = nonce;
    apickli.headers['timestamp'] = timestamp;
    apickli.headers['Authorization'] = hmac;

   // apickli.get(pathSuffix, callback);
	apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);				
			}
			  apickli.storeValueOfResponseBodyPathInScenarioScope('$.items[1].reference', 'batchReference');		
				  console.log("Batch RefNumber : "+ apickli.scenarioVariables.batchReference);
				  var Reference = apickli.scenarioVariables.Reference;
			
        	callback();
		});
};

const getBatchSummary = (apickli, batchReference, callback) => {
    var pathSuffix = "/batches/"+batchReference+"/summary";
    var url = apickli.domain + pathSuffix;

    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac(clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.headers['nonce'] = nonce;
    apickli.headers['timestamp'] = timestamp;
    apickli.headers['Authorization'] = hmac;

    apickli.get(pathSuffix, callback);
};



module.exports = function () { 

	this.Given(/^I create a sale transaction$/, function (callback) {
		createSale(this.apickli, callback);
	});
	
	this.When(/^I settle all current set of transaction$/, function (callback) {
       
		batchSettle(this.apickli,callback);
	});
	
	this.When(/^I get the all Batch Details$/, function (callback) {
        var batchReference = this.apickli.scenarioVariables.batchReference;
          console.log("REFERENCE: ", batchReference );
        getBatch(this.apickli, batchReference, callback);
    });
	
	this.When(/^I get that same batch summary$/, function (callback) {
        var batchReference = this.apickli.scenarioVariables.batchReference;
          console.log("REFERENCE: ", batchReference );
        getBatchSummary(this.apickli, batchReference, callback);
    });

	};
