var apickli = require('apickli');
var config = require('../../config/config.json');

var defaultBasePath = config.ach.basepath;
var defaultDomain = config.ach.domain;

console.log('ach api: [' + config.ach.domain + ', ' + config.ach.basepath + ']');

var getNewApickliInstance = function(basepath, domain) {
	basepath = basepath || defaultBasePath;
	domain = domain || defaultDomain;

	return new apickli.Apickli('https', domain + basepath);
};

exports.getNewApickliInstance = getNewApickliInstance;
