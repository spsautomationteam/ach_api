@intg
Feature: Get Credits
    As an API consumer
    I want to query credit requests
    So that I know they have been processed

    @get-All-CreditTransactions
    Scenario: query all credits 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set content-type header to application/json
		When I create a sale charge
        Then response code should be 201
		When I get the list of credit transactions
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
		And response body should contain Credit
		And response body should contain "paymentType":"ACH"
		And response body should contain "service":"ACH"
		And response body should contain "type":"Credit"        
        And response body should not contain Bankcard
		And response body should not contain "type":"Sale"
        And response body should not contain transactionCode 		

	@get-Credits_byStartDate
    Scenario: query all credits by StartDate
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?StartDate=2017-07-30
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain Sale
        And response body should contain Credit
        And response body should contain "startDate":"2017-07-30"
		
	@get-Credits_byEndDate
    Scenario: query all credits by End Date
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?EndDate=2017-07-18
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain Sale
        And response body should contain Credit
        And response body should contain "endDate":"2017-07-18"		
                                
	@get-Credits_byPageNumber
    Scenario: query all credits by PageNumber
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?pageNumber=1
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain Sale
        And response body should contain Credit
        And response body should contain "pageNumber":1		
		
	@get-Credits_byPageSize
    Scenario: query all credits by Page size
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?pageSize=3
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain Sale
        And response body should contain Credit
        And response body should contain "pageSize":3		
		
	@get-Credits_bysortDescending-sortField
    Scenario: query all credits by Sorting set to descending
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?sortDirection=Descending&sortField=date
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain Sale
        And response body should contain Credit
        And response body should not contain transactionCode
		
	@get-Credits_bysorAscending-sortField
    Scenario: query all credits by Sorting set to Ascending
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?sortDirection=Ascending&sortField=date
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain Sale
        And response body should contain Credit
        And response body should not contain transactionCode
		
	@get-Credits_byIsPurchaseCard
    Scenario: query all credits by ispurchase card status
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?isPurchaseCard=false
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain Sale
        And response body should contain Credit
		And response body should not contain "isPurchaseCard"=false
		
	@get-Credits_byAccountNumber
    Scenario: query all credits by account number
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?accountNumber=7890
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
		And response body should contain Credit
        And response body should not contain Sale       
        And response body should contain "accountNumber":"XXXXXX7890"		
		
	@get-Credits_byOrderNumber
    Scenario: query all credits by order number
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set content-type header to application/json
		When I create a credit transaction
        Then response code should be 201
        And response body path $.status should be Approved
        When I use HMAC and GET /credits?reference=`creditOrderNumber`
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path $.orderNumber should be `orderNumber`
		
	@get-Credits_byReference
    Scenario: query all credits by reference
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set content-type header to application/json
		When I create a credit transaction
        Then response code should be 201
        And response body path $.status should be Approved
        When I use HMAC and GET /credits?reference=`creditReference`
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path $.reference should be `reference`        
		
	@get-Credits_byTotalAmount
    Scenario: query all credits by total amount
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set content-type header to application/json
		When I create a credit transaction
        Then response code should be 201
        And response body path $.status should be Approved
        When I use HMAC and GET /credits?totalAmount=100
        Then response code should be 200
        And response header Content-Type should be application/json    
		And response body path $.items[0].amounts.total should be 100
		
	@get-Credits_byApprovalCode
    Scenario: query all credits by approval code
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set content-type header to application/json
		When I create a credit transaction
        Then response code should be 201
        And response body path $.status should be Approved
        When I use HMAC and GET /credits?code=`creditCode`
        Then response code should be 200
        And response header Content-Type should be application/json    
		And response body path $.responseCode should be `creditCode`
		
       
	###################### Negative scenarios ###########################

	@get-Credits_tryToGetSaleTransDetails
    Scenario: query to get Sale details from Credit API
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set content-type header to application/json
		When I create a sale charge
        Then response code should be 201
		When I get that same sale charge
		Then response code should be 404
        And response body path $.code should be 000000
		And response body path $.message should be Missing or invalid Application Identifier
		And response body path $.detail should be A valid Application ID is required in header parameter clientId.	

	@get-Credits-StartDate_AsFutureDate
    Scenario: Verify the API Get Credits with Future StartDate
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?StartDate=2030-12-30
        Then response code should be 200
        And response body should contain "startDate":"2030-12-30"
		And response body should not contain "reference"
		And response body should not contain "service"
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0

	@get-Credits-EndDate_AsPastDate
     Scenario: Verify the API Get Credits with Past EndDate
         Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?EndDate=2012-12-12
        Then response code should be 200
        And response body should contain "endDate":"2012-12-12"
		And response body should not contain "reference"
		And response body should not contain "service"
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0	
		
	@get-Credits_byInvalidPageSize
    Scenario: query all credits by InvalidPageSize
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?pageSize=abcdef
        Then response code should be 200
	
	@get-Credits_byInvalidPageNumber
    Scenario: query all credits by InvalidPageNumber
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?pageNumber=abcdef
        Then response code should be 200
		
	@get-Credits_byInvalidSortDirection
    Scenario: query all credits by InvalidSortDirection
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?sortDirection=Testing
        Then response code should be 200
		
	@get-Credits_byInvalidSortField
    Scenario: query all credits by InvalidSortField
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?sortField=TEST
        Then response code should be 400
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be InvalidRequestData : SortField must be a field in the result set		
		
	@get-Credits_byInvalidName
    Scenario: query all credits by Invalidname
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?name=qwertsdfgsd
        Then response code should be 200
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0	
		
	@get-Credits_byInvalidAccountNum
    Scenario: query all charges by InvalidAccount
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?accountNumber=qwer
        Then response code should be 200
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0	
		
	@get-Credits_byInvalidSource
    Scenario: query all charges by InvalidSource
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?source=qwer
        Then response code should be 200	
		
	@get-Credits_byInvalidOrderNumber
    Scenario: query all credits by InvalidorderNumber
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?orderNumber=abcabcabcabc
        Then response code should be 200
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0	
		
	@get-Credits_byInvalidReference
    Scenario: query all credits by InvalidReference
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?reference=ABCABCABC
        Then response code should be 200
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0			
			
	@get-Credits_byInvalidBatchRef
    Scenario: query all credits by InvalidBatchRef
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?batchReference=ABCABCABC
        Then response code should be 200
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0			
			
	@get-Credits_byInvalidTotalAmount_WithLongAmount
    Scenario: query all credits by InvalidTotalAmount
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?totalAmount=213123123123123123
        Then response code should be 503
		And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.
		
	@get-Credits_byInvalidTotalAmount_WithMoreThan2Digits
    Scenario: query all credits by InvalidTotalAmount
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?totalAmount=999
        Then response code should be 200
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0		

	@get-Credits_byInvalidTotalAmount_WithText
    Scenario: query all credits by InvalidTotalAmount
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /credits?totalAmount=Test
        Then response code should be 200
					
			
	

