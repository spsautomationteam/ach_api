@intg
Feature: Put Token (update the card data associated with a vault token.)
	As an API consumer
	I want to update the card data associated with a vault token.
	So that I know they can be processed

    @put-token
    Scenario Outline: update the card data associated with a vault token of different account type
		Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": <accountType>,"routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 200
		When I update card data for the above token with body set to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 200  
		And response body path $.vaultResponse.data should be `token`  		
	Examples:
	|accountType|
	|"Savings"|
	|"Checking"|
	
	@put-token-withoutAccountTag
    Scenario: Store a card data without account type 
        Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": "Checking","routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 200
		When I update card data for the above token with body set to {{"type": "Checking","routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 400 
		And response body path $.detail should be Invalid JSON request in message request, check syntax
	
	
	@put-token-withoutAccountType
    Scenario: update the card data associated with a vault token without account type 
        Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": "Checking","routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 200
		When I update card data for the above token with body set to {"account": {"routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 400  
		     

	@put-token-withoutAccountNumber
    Scenario: update the card data associated with a vault token without account number data
        Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": "Checking","routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 200
		When I update card data for the above token with body set to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": ""}}
        Then response code should be 400
	
	@put-token-withInvalidAccountNumber
    Scenario: update the card data associated with a vault token with invalid account number
        Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": "Checking","routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 200
		When I update card data for the above token with body set to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "1234"}}
        Then response code should be 401
			
	@put-token-withoutAccountNumberTag
    Scenario:update the card data associated with a vault token  without account number tag
        Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": "Checking","routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 200
		When I update card data for the above token with body set to {"account": {"type": "Savings","routingNumber": "056008849"}}
        Then response code should be 400
		And response body path $.message should be Invalid message request content
	#	And response body path $.detail should be Required content: account(routingNumber, accountNumber, type)
		And response body path $.detail should be Required content: account

	@put-token-withoutRoutingNumber
    Scenario: update the card data associated with a vault token without routing number data
        Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": "Checking","routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 200
		When I update card data for the above token with body set to {"account": {"type": "Savings","routingNumber": "","accountNumber": "12345678901234"}}
        Then response code should be 400
		
	@put-token-withInvalidRoutingNumber
    Scenario: update the card data associated with a vault token with invalid routing number data
        Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": "Checking","routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 200
		When I update card data for the above token with body set to {"account": {"type": "Savings","routingNumber": "123123123","accountNumber": "12345678901234"}}
        Then response code should be 401
		And response body path $.detail should be You are a using certification environment which is restricted to test data only.
		
	@put-token-withoutRoutingNumberTag
    Scenario: update the card data associated with a vault token without routing number tag
        Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": "Checking","routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 200
		When I update card data for the above token with body set to {"account": {"type": "Savings","accountNumber": "12345678901234"}}
        Then response code should be 400
		And response body path $.message should be Invalid message request content
	#	And response body path $.detail should be Required content: account(routingNumber, accountNumber, type)
		And response body path $.detail should be Required content: account
		
	@put-token-withNullBodyData
    Scenario: update the card data associated with a vault token without sending request body
       Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": "Checking","routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 200
		When I update card data for the above token with body set to {}
        Then response code should be 400
		And response body path $.message should be Invalid message request content
	#	And response body path $.detail should be Required content: account(routingNumber, accountNumber, type)
		And response body path $.detail should be Required content: account
   
	@put-token-withInvalidTokenReference
    Scenario: update the card data associated with a vault token with invalid token reference
        Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": "Checking","routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 200
		When I try to update card data by invalid token reference dfsdfsfsdfsdfsdfsdfsdf with body set to {"account": {"type": "Checking","routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 404
	
		
		