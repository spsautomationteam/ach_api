@intg
Feature: GET_Batches_Reference_Summary
    As an API consumer
    I want to query batches summery with reference
    So that I know they have been processed

	@get-Batches-Reference-Summery_byGetBatchReference
	Scenario: Verify retrieve summarized information, such as count and volume, about the transactions in the current batch.
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I get all Batch Transaction Details
        Then response code should be 200
		When I get Batche Summary Details With GetBatch Reference Number
		Then response code should be 200
		And response body should contain "paymentType"
		And Verify Get Batches 'Count' and 'Volumes' values Matching at Get Batches Summary Reference API		

	############# Negative Scenarios  #####################		

		
	@get-Batches-Reference-Summery_byinValidReference
	Scenario: Try to get transaction summery with invalid Reference 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /batches/Test1234/summary
		Then response code should be 200
		And response body should not contain "paymentType"
		And response body should contain "authCount":0
		And response body should contain "authTotal":0
		And response body should contain "saleCount":0
		And response body should contain "saleTotal":0
		And response body should contain "creditCount":0
		And response body should contain "creditTotal":0
		And response body should contain "totalCount":0
		And response body should contain "totalVolume":0



 