@intg
Feature: Delete Credits 
	As an API consumer
	I want to delete/void credit requests based on the reference
		
	@delete-Credits_byCreditTxReference
    Scenario: delete credit from reference of transaction
        Given I have valid credentials
        And I set content-type header to application/json
        When I perform a credit charge
        Then response code should be 201
        And response body path $.status should be Approved
        When I delete that same credit
        Then response code should be 200   
		

	################### Negative Scenarios ########################		

	
	@delete-Credits_byAlreadyDeleted-CreditTx
    Scenario: delete charge from reference of Sale,Auth and force tranaction
        Given I have valid credentials
        And I set content-type header to application/json
        When I perform a credit charge
        Then response code should be 201
        And response body path $.status should be Approved
        When I delete that same credit
        Then response code should be 200  
		When I delete that same credit
        Then response code should be 404
   		And response body path $.message should be Internal Server Error     
	
	@delete-Credits_byInvalidReference
    Scenario: Try to delete credit by entering wrong reference
        Given I have valid credentials
        And I set content-type header to application/json       
        When I delete the credit with invalid reference TEST124
        Then response code should be 404  
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.
     
    #try to delete a settled transaction
	
	
		