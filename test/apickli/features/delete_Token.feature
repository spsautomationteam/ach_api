@intg
Feature:  Delete a Vault Token
	As an API consumer
	I want to delete vault token
		
	@delete-Token_referenced-token-AllAccountType
    Scenario Outline: delete a token generated for different account type
        Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": <accountType>,"routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 200
        When I delete that same token
        Then response code should be 200  
		And response body path $.vaultResponse.status should be 1  
		And response body path $.vaultResponse.message should be DELETED 
		And response body path $.vaultResponse.data should be `token`
		
    Examples:
	|accountType|
	|"Savings"|
	|"Checking"|
	
	########### Negative Scenarios #############
	
	@delete-Token_byAlready-DeletedToken
    Scenario: Try to delete already delated token 
        Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": "Checking","routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 200
        When I delete that same token
        Then response code should be 200   
		When I delete that same token
        Then response code should be 404
  		
	@delete-Token_byInvalid-TokenReference
    Scenario: Try to delete token by entering wrong reference
        Given I have valid credentials		
        When I delete the token with invalid reference 2erwwerwer235235325swfs
        Then response code should be 404  
		And response body path $.code should be 100006
		And response body path $.message should be Resource not found
		And response body path $.detail should be UNABLE TO LOCATE
		
		
	
     
	
	
    