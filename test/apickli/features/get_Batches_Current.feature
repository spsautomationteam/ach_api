@intg
Feature: Get Batches Current
    As an API consumer
    I want to query batches details requests
    So that I know they have been processed

    @get-all-CurrentBatches
    Scenario: query current batches
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /batches/current?pageSize=10
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain transactionCode
		
	@get-batchCurrent-byPageNo
    Scenario: query current batch by page no 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /batches/current?pageNumber=10
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain transactionCode

	@get-Batches-byPageSize
    Scenario: query batch by page size 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /batches/current?pageSize=1
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body should contain "pageSize":1
		And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain transactionCode		
		
	@get-batchCurrent-bySortDirectionAsc
    Scenario: query current batch by ascending
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /batches/current?sortDirection=ascending
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain transactionCode	

	@get-batchCurrent-bySortDirectionDesc
    Scenario: query current batch by descending order
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /batches/current?sortDirection=descending
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain transactionCode			
		
		 