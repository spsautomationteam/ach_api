@intg
Feature: Get Transactions Details
    As an API consumer
    I want to query transactions details requests
    So that I know they have been processed

	 @get-Transactions-Details_bySaleReference
     Scenario: query specific transaction details
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a sale charge
        Then response code should be 201
        And response body path $.status should be Approved
        When I use HMAC and GET /transactions/`saleReference`
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path $.reference should be `saleReference`
        And response body path $.amounts.total should be 1
		And response body should not contain Bankcard
        And response body should not contain transactionCode
        And response body should contain Sale
        And response body should not contain Credit
		
	@get-Transactions-Details_byCreditTransactions
    Scenario: query all credit transactions
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set content-type header to application/json
		 When I perform a credit charge
        Then response code should be 201
        And I use HMAC and GET /transactions/`creditReference`
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body path $.reference should be `creditReference`
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain transactionCode
        And response body should contain Credit
        And response body should not contain Sale
		
	###################### Negative scenarios #########################	

	@get-Transactions-Details_byInvalidReference
     Scenario: query specific transaction details 
        Given I have valid credentials
        And I set content-type header to application/json       
        When I use HMAC and GET /transactions/ABCABCABC
		Then response code should be 404
		And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.

		