@intg
Feature: Delete_Charges 
	As an API consumer
	I want to delete/void charge requests based on the reference
		
#	===>>>>>> Commented below code it's required clarification

#	@delete-Charges_byReferenced-AllTransactions
#    Scenario Outline: delete charge from reference of Sale,Auth and force tranaction
#        Given I have valid credentials
#        And I set content-type header to application/json
#        When I create a <TypeOfTx> charge
#        Then response code should be 201
#        And response body path $.status should be Approved
#        When I delete that same charge
#        Then response code should be 200       
#    Examples:
#	|TypeOfTx|
#	|Sale|
#	|Force|
#	|Authorization|
#	|Credit |
	
	@delete-Charges_byReferenced-Sale
    Scenario: delete charge from reference of Sale tranaction
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a Sale charge
        Then response code should be 201
        And response body path $.status should be Approved
        When I delete that same charge
        Then response code should be 200      
  
	
	################ Negative Scenarios #######################
	
#	****** Commented below code it's required clarification
	@delete-Charges_byReferenced-Force
    Scenario: delete charge from reference of Auth tranaction
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a Force charge
        Then response code should be 201
        And response body path $.status should be Approved       
		When I delete that same charge       
		Then response code should be 404
        And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.
		
	@delete-Charges_byReferenced-Auth
   Scenario: delete charge from reference of Auth tranaction
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a Authorization charge
        Then response code should be 201
        And response body path $.status should be Approved       
		When I delete that same charge       
		Then response code should be 404
        And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.
	
	@delete-Charges_byAlready-DeletedCharge
    Scenario: Try to delete already delated charge 
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a Sale charge
        Then response code should be 201
        And response body path $.status should be Approved
        When I delete that same charge
        Then response code should be 200  
		When I delete that same charge       
		Then response code should be 404
        And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.
		
	@delete-Charges_byinvalid_TxReference
    Scenario: Try to delete charge by entering wrong reference
        Given I have valid credentials
        And I set content-type header to application/json  
        When I delete the charge with invalid reference ABCABCABC 
        Then response code should be 404
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.
		
		
    #try to delete a settled transaction
     
	
	
    