@intg
Feature: Post Token
	As an API consumer
	I want to store a card and retrieve a vault token
	So that I know they can be processed

    @post-Token
    Scenario Outline: Store a card data for differenct account type and retrieve a token
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
	#	And I set body to {"account": {"type": <accountType>,"routingNumber": "056008849","accountNumber": "12345678901234"} }
	#	When I use HMAC and POST to /tokens
		When I create a vault token with body set to {"account": {"type": <accountType>,"routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body path $.vaultResponse.status should be 1  
		And response body path $.vaultResponse.message should be SUCCESS 
		And response body path $.vaultResponse.data should be `token`
        
	Examples:
	|accountType|
	|"Savings"|
	|"Checking"|
	
	
	############ Negative Scenarios  ################
	
	@post-Token_withoutAccountTag
    Scenario: Store a card data without account type 
        Given I have valid credentials
		When I create a vault token with body set to {{"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 400 
		And response body path $.detail should be Invalid JSON request in message request, check syntax
	
	@post-Token_withoutAccountType
    Scenario: Store a card data without account type 
        Given I have valid credentials
		When I create a vault token with body set to {"account": {"routingNumber": "056008849","accountNumber": "12345678901234"}}
        Then response code should be 400      

	@post-Token_withoutAccountNumber
    Scenario: Store a card data without account number data
        Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": ""}}
        Then response code should be 400
	
	@post-Token_withInvalidAccountNumber
    Scenario: Store a card data with invalid account number
        Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "1234"}}
        Then response code should be 401
		And response body path $.detail should be You are a using certification environment which is restricted to test data only.
		
	@post-Token_withoutAccountNumberTag
    Scenario: Store a card data without account number tag
        Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": "Savings","routingNumber": "056008849"}}
        Then response code should be 400    

	@post-Token_withoutRoutingNumber
    Scenario: Store a card data without routing number data
        Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": "Savings","routingNumber": "","accountNumber": "12345678901234"}}
        Then response code should be 400
		
	@post-Token_withInvalidRoutingNumber
    Scenario: Store a card data with invalid routing number
        Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": "Savings","routingNumber": "123123123","accountNumber": "12345678901234"}}
        Then response code should be 401
		And response body path $.detail should be You are a using certification environment which is restricted to test data only.
		
		
	@post-Token_withoutRoutingNumberTag
    Scenario: Store a card data without routing number tag
        Given I have valid credentials
		When I create a vault token with body set to {"account": {"type": "Savings","accountNumber": "12345678901234"}}
        Then response code should be 400
		
	@post-Token_withNullBodyData
    Scenario: Store a card data without sending request body
        Given I have valid credentials
		When I create a vault token with body set to {}
        Then response code should be 400
   
	