@intg
Feature: Get Transactions
    As an API consumer
    I want to query transactions requests
    So that I know they have been processed

    @get-Transaction_getAllTransactions
    Scenario: query to get all transactions
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /transactions
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain transactionCode

	 @get-Transaction_bySaleTxReference
     Scenario: query Sale transaction with reference 
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a sale charge
        Then response code should be 201
        And response body path $.status should be Approved
        When I use HMAC and GET /transactions/`saleReference`
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path $.reference should be `saleReference`
        And response body path $.amounts.total should be 1
		And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain transactionCode
        And response body should contain Sale
        And response body should not contain Credit		
		
	 @get-Transaction_byCreditTxReference
     Scenario: query Credit transaction with reference 
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a credit transaction
        Then response code should be 201
        And response body path $.status should be Approved
        When I use HMAC and GET /transactions/`creditReference`
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path $.reference should be `creditReference`
        And response body path $.amounts.total should be 1
		And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain transactionCode
        And response body should contain Credit
        And response body should not contain Sale
		
	@get-Transaction_byStartDate
    Scenario: query by Start Date
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /transactions?startDate=01-01-2017
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body should contain 2017-01-01
		
	@get-Transaction_byEndDate
    Scenario: query by End Date Transaction
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /transactions?endDate=01-01-2017
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body should contain 2017-01-01	

	@get-Transaction_byPageNo
    Scenario: query by page no
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /transactions?pageNumber=10
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain transactionCode
		
	@get-Transaction_bySortDirectionAsc
    Scenario: query Transaction by order
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /transactions?sortDirection=ascending
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain transactionCode	

	@get-Transaction_bySortDirectionDesc
    Scenario: query Transaction by order
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /transactions?sortDirection=descending
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain transactionCode	

	@get-Transaction_bySortField
    Scenario: query by Sort field
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /transactions?sortField=date
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain transactionCode	

	@get-Transaction_byisPurchaseCard
    Scenario: query by purchase card
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /transactions?isPurchaseCard=true
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain transactionCode	

	@get-Transaction_byisPurchaseCard
    Scenario: query by purchase card
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /transactions?isPurchaseCard=false
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain transactionCode		

	@get-Transaction_byName
    Scenario: query by Sort by name
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set content-type header to application/json
		When I create a credit transaction
        And I use HMAC and GET /transactions?name=jean-luc 		
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body should contain jean-luc picard
		
	@get-Transaction_byAccountNumber
    Scenario: query by account Number
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set content-type header to application/json
		When I create a Sale charge	
		Then response code should be 201		
		And I get that same Sale charge 
      # When I use HMAC and GET /transactions?AccountNumber=`accountNumber`
        Then response code should be 201
      # And response body should contain "number":`number`	
		And response header Content-Type should be application/json  
		
	
	@get-Transaction_byOrderNo
    Scenario: query by Order Number
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set content-type header to application/json
		When I create a credit transaction
		Then response code should be 201
        And I use HMAC and GET /transactions?type=Credit        
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body path $.orderNumber should be `orderNumber`
		
	@get-Transaction_byReference
    Scenario: query by Reference Number
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set content-type header to application/json
		When I create a credit transaction
        And I use HMAC and GET /transactions?reference=`creditReference`        
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body path $.reference should be `reference`	

	@get-Transaction_byTotalAmount
    Scenario: query by account Number
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set content-type header to application/json
		When I create a credit transaction
        And I use HMAC and GET /transactions?totalAmount=100      
        Then response code should be 200
        And response header Content-Type should be application/json		
		And response body path $.items[0].amounts.total should be 100	
		
	# +++++++++++++++++    Negative Scinarios   ++++++++++++++++++++++++++

    @get-Transactions_byInvalidReference
    Scenario: query transactions with invalid reference
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set content-type header to application/json		
        And I use HMAC and GET /transactions?reference=ABCABCABC
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0		
		
	@get-Transactions_StartDate_AsFutureDate
     Scenario: Verify the API Get Transactions with Future StartDate
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?StartDate=2030-12-12
        Then response code should be 200
        And response body should contain "startDate":"2030-12-12"
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0		
		
	@get-Transactions_EndDate_AsPastDate
     Scenario: Verify the API Get Transactions with Past EndDate
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?EndDate=2012-12-12
        Then response code should be 200
        And response body should contain "endDate":"2012-12-12"
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0
		
	@get-Transactions_byinValidGatewayID
     Scenario: Verify the API Get transactions with inValidGatewayId
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?gatewayId=fsfddssdfsdf
        Then response code should be 200
        And response body should not contain "gatewayId":"fsfddssdfsdf"  
		
	@get-Transaction_invalidTransaction
    Scenario: query invalid Transaction
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /transactions/xyz
        Then response code should be 404
        And response header Content-Type should be application/json
        And response body should not contain ACH
		
	@get-Transactions_byinValidOrderNumber
     Scenario: Verify the API Get transactions with inValidOrderNumber
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?orderNumber=FSFFFDFSDFSDDFF
        Then response code should be 200
		And response body should not contain "orderNumber":"`FSFFFDFSDFSDDFF`"
	
	@get-Transactions_byinValidApprovalCode
     Scenario: Verify the API Get transactions with inValidApprovalCode
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?approvalCode=FSFFFDFSDFSDDFF
        Then response code should be 200
        And response body should not contain "responseCode":"`FSFFFDFSDFSDDFF`"
	
	@get-Transactions_byinValidStatus
     Scenario: Verify the API Get transactions with inValidStatus
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?Status=fkjsdklfdsjsfdf
        Then response code should be 200
        And response body should not contain "status":"fkjsdklfdsjsfdf"
	
	@get-Transactions_byinValidSource
     Scenario: Verify the API Get transactions with inValidSource
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?Source=FSDFDSFDSFSFSSF
        Then response code should be 200
        And response body should not contain "source":"FSDFSDSFFSDFS"		
	
	@get-Transactions_byinValidIsPurchaseCard
     Scenario: Verify the API Get transactions with inValidPurchaseCard
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?IsPurchaseCard=FSDFDSFDSFSFSSF
        Then response code should be 200
        And response body should not contain "IsPurchaseCard"	
		
	@get-Transactions_byinValidTotalAmount
     Scenario: Verify the API Get transactions with inValidTotalAmount
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?totalAmount=999
        Then response code should be 200
        And response body should not contain "source":"FSDFSDSFFSDFS"
		And response body path $.summary[0].paymentType should be ACH
		And response body path $.summary[0].authCount should be 0
		And response body path $.summary[0].authTotal should be 0
		And response body path $.summary[0].saleCount should be 0
		And response body path $.summary[0].saleTotal should be 0
		And response body path $.summary[0].creditCount should be 0
		And response body path $.summary[0].creditTotal should be 0
		And response body path $.summary[0].totalCount should be 0
		And response body path $.summary[0].totalVolume should be 0		
		
		
		
		
		
		
	