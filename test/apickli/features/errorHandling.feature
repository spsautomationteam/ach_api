@intg
@Error-Handling
Feature: Error handling
	As an API consumer
	I want consistent and meaningful error responses
	So that I can handle the errors correctly

	@invalid-hmac
    Scenario: GET with invalid hmac
        Given I set clientId header to `clientId`
        And I set merchantId header to 12345
        And I set merchantKey header to 67890
        And I set nonce header to `nonce`
        And I set timestamp header to `timestamp`
        And I set Authorization header to invalid-hmac
        When I GET /ping
        Then response code should be 401
        And response header Content-Type should be application/json
        And response body path $.code should be 100003
        And response body path $.message should be Invalid HMAC

	@invalid-client-id
    Scenario: GET with invalid clientId
        Given I set clientId header to badapikey
        When I GET /ping
        Then response code should be 401
        And response header Content-Type should be application/json
        And response body path $.code should be 100005
        And response body path $.message should be Missing or invalid Application Identifier

	@missing-client-id
    Scenario: GET with missing clientId
        When I GET /ping
        Then response code should be 401
        And response header Content-Type should be application/json
        And response body path $.code should be 100005
        And response body path $.message should be Missing or invalid Application Identifier

    @invalid-clientid-for-resource
    Scenario: GET with invalid clientId for resource
        Given I set clientId header to `invalidClientId`
        When I GET /ping
        Then response code should be 401
        And response header Content-Type should be application/json
        And response body path $.code should be 100005
        And response body path $.message should be Missing or invalid Application Identifier

	@missing-merchant-id
    Scenario: GET with missing required header merchantId
        Given I set clientId header to `clientId`
        And I set merchantKey header to 67890
        And I set nonce header to `nonce`
        And I set timestamp header to `timestamp`
        And I set Authorization header to invalid-hmac
        When I GET /ping
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 100001
        And response body path $.message should be One or more required headers are missing

	@missing-merchant-key
    Scenario: GET with missing required header merchantKey
        Given I set clientId header to `clientId`
        And I set merchantId header to 12345
        And I set nonce header to `nonce`
        And I set timestamp header to `timestamp`
        And I set Authorization header to invalid-hmac
        When I GET /ping
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 100001
        And response body path $.message should be One or more required headers are missing

	@missing-nonce
    Scenario: GET with missing required header nonce
        Given I set clientId header to `clientId`
        And I set merchantId header to 12345
        And I set merchantKey header to 67890
        And I set timestamp header to `timestamp`
        And I set Authorization header to invalid-hmac
        When I GET /ping
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 100001
        And response body path $.message should be One or more required headers are missing

	@missing-timestamp
    Scenario: GET with missing required header timestamp
        Given I set clientId header to `clientId`
        And I set merchantId header to 12345
        And I set merchantKey header to 67890
        And I set nonce header to `nonce`
        And I set Authorization header to invalid-hmac
        When I GET /ping
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 100001
        And response body path $.message should be One or more required headers are missing

	@missing-authorization
    Scenario: GET with missing required header Authorization
        Given I set clientId header to `clientId`
        And I set merchantId header to 12345
        And I set merchantKey header to 67890
        And I set nonce header to `nonce`
        And I set timestamp header to `timestamp`
        When I GET /ping
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 100001
        And response body path $.message should be One or more required headers are missing

	@foo
    Scenario: GET /foo request
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /foo
        Then response code should be 404
        And response header Content-Type should be application/json
        And response body path $.code should be 100006
        And response body path $.message should be Resource not found
        
	@foobar
    Scenario: GET /foo/bar request
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /foo/bar
        Then response code should be 404
        And response header Content-Type should be application/json
        And response body path $.code should be 100006
        And response body path $.message should be Resource not found
        

    @invalid-json
    Scenario: POST charge with invalid content
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to {'hello':'world}
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 100007
        And response body path $.message should be Invalid message request format
        And response body path $.detail should be Invalid JSON request in message request, check syntax

    @wrong-content-type
    Scenario: POST charge with wrong content-type
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to text/xml
        And I set body to {"hello":"world"}
        When I POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 100007
        And response body path $.message should be Invalid message request format
        And response body path $.detail should be Invalid JSON request in message request, check syntax
    
    @missing-content-type
    Scenario: POST charge with missing content-type
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set body to {"hello":"world"}
        When I POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 100001
        And response body path $.message should be One or more required headers are missing
		
		
	
		
	###################################Error code######################
		
	@SAGE-ACH-API-NBSpec-Errors-000000InternalServerError 
    Scenario: SAGE ACH API - NB Spec - Errors - 000000 Internal Server Error 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
     	When I use HMAC and POST to /credits/ping
		Then response code should be 503
		And response body path $.message should be Internal Server Error
		And response body path $.code should be 000000 
		
	#@SAGETokenAPINBSpec-Errors-100000ServiceUnavailable 
     #Scenario: SAGE ACH API - NB Spec - Errors - 100000 Service Unavailable 
      #  Given I set clientId header to `clientId`
       # And I set merchantId header to `merchantId`
        #And I set merchantKey header to `merchantKey`
		#And I set Content-Type header to "application/json"
		#And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
     	#When I use HMAC and POST to /credits/ping
		#Then response code should be 503
		#And response body path $.message should be Service is currently not available
		#And response body path $.code should be 100000 
		
	@SAGE-ACH-API-NBSpec-Errors-100001HeaderParams 
        Scenario: SAGE ACH API - NB Spec - Errors - 100001 Header Params 
        Given I set clientId header to `clientId`
        And I set merchantKey header to 67890
        And I set nonce header to `nonce`
        And I set timestamp header to `timestamp`
        And I set Authorization header to invalid-hmac
        When I GET /ping
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 100001
        And response body path $.message should be One or more required headers are missing
		
	#@SAGE-ACH-API-NBSpec-Errors-100002QueryParams 
        #Scenario: SAGE ACH API - NB Spec - Errors - 100002 Query Params 
        #Given I set clientId header to `clientId`
        #And I set merchantId header to `merchantId`
        #And I set merchantKey header to `merchantKey`
		#And I set Content-Type header to "application/json"
		#And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
     	#When I use HMAC and POST to /charges?type
		#Then response code should be 400
		#And response body path $.message should be One or more required query parameters are missing 
		#And response body path $.code should be 100002 
		
	@SAGEACHAPI-NBSpec-Errors-100003InvalidHMAC 
        Scenario: SAGE ACH API - NB Spec - Errors - 100003 Invalid HMAC 
        Given I set clientId header to `clientId`
        And I set merchantId header to 12345
        And I set merchantKey header to 67890
        And I set nonce header to `nonce`
        And I set timestamp header to `timestamp`
        And I set Authorization header to invalid-hmac
        When I GET /ping
        Then response code should be 401
        And response header Content-Type should be application/json
        And response body path $.code should be 100003
        And response body path $.message should be Invalid HMAC
		
	@SAGEACHAPI-NBSpec-Errors-100005InvalidclientId 
        Scenario: SAGE ACH API - NB Spec - Errors - 100005 Invalid clientId 
        Given I set clientId header to `invalidClientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
	 	When I use HMAC and POST to /credits
		Then response code should be 401
		And response body path $.message should be Missing or invalid Application Identifier 
		And response body path $.code should be 100005 
		
	@SAGEACHAPI-NBSpec-Errors-100006ResourceNotFound 
        Scenario: SAGE ACH API - NB Spec - Errors - 100006 Resource Not Found 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
     	When I use HMAC and POST to /creditss
		Then response code should be 404
		And response body path $.message should be Resource not found 
		And response body path $.code should be 100006 
		
	@SAGEACHAPI-NBSpec-Errors-100007InvalidMessageFormat 
        Scenario: SAGE ACH API - NB Spec - Errors - 100007 Invalid Message Format 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        And I set content-type header to application/json
        And I set body to {'hello':'world}
        When I use HMAC and POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 100007
        And response body path $.message should be Invalid message request format
        And response body path $.detail should be Invalid JSON request in message request, check syntax
		
		
	#@SAGEACHAPI-NBSpec-Errors-400000InvalidRequest 
        #Scenario: SAGE ACH API - NB Spec - Errors - 400000 Invalid Request
        #Given I set clientId header to `clientId`
        #And I set merchantId header to `merchantId`
        #And I set merchantKey header to `merchantKey`
		#And I set Content-Type header to "application/json"
		#And I set body to {     "deviceId": "",     "secCode": "",     "originatorId": "",     "amounts": {         "total": 0,         "tax": 0,         "shipping": 0     },     "account": {         "type": "",         "routingNumber": "",         "accountNumber": "",         "priorReference": ""     },     "checkNumber": 0,     "customer": {         "dateOfBirth": "",         "ssn": "",         "license": {             "number": "",             "stateCode": ""         },         "ein": "",         "email": "",         "telephone": "",         "fax": ""     },     "billing": {         "name": {             "first": "",             "middle": "",             "last": "",             "suffix": ""         },         "address": "",         "city": "",         "state": "",         "postalCode": "",         "country": ""     },     "shipping": {         "name": "",         "address": "",         "city": "",         "state": "",         "postalCode": "",         "country": ""     },     "orderNumber": "",     "addenda": "",     "isRecurring": false,     "recurringSchedule": {         "amount": 0,         "frequency": "",         "interval": 0,         "nonBusinessDaysHandling": "",         "startDate": "",         "totalCount": 0,         "groupId": ""     },     "vault": {         "token": "",         "operation": ""     },     "memo": "" }
     	#When I use HMAC and POST to /credits?type=Authorization
		#Then response code should be 400
		#And response body path $.message should be There was a problem with the request
		#And response body should contain InvalidRequestData 
		#And response body path $.code should be 400000 
		
		
		#--------------------------------SAGE - Errors validated under 400000 ------------------------------------------------------------------------

	 @SAGEACHAPI-NBSpec-Errors-400000InvalidAddress
        Scenario: SAGE ACH API-NBSpec-Errors-400000 Invalid Address
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address":  "", "city": "cityville", "state": "va", "postalCode": "12345"} }
     	When I use HMAC and POST to /charges?type=sale
		Then response code should be 400
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more
		And response body path $.detail should be InvalidRequestData : request.Billing: Address is required
		And response body path $.code should be 400000 	
	
	@SAGEACHAPI-NBSpec-Errors-400000InvalidCity 
        Scenario: SAGE ACH API - NB Spec - Errors - 400000 Invalid City 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address":  "1234 street", "state": "va", "postalCode": "12345"} }
     	When I use HMAC and POST to /charges?type=sale
		Then response code should be 400
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more
		And response body path $.code should be 400000 	
        And response body path $.detail should be InvalidRequestData : request.Billing: City is required
		
	@SAGEACHAPI-NBSpec-Errors-400000InvalidState 
        Scenario: SAGE ACH API - NB Spec - Errors - 400000 Invalid State 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address":  "1234 street","city": "cityville","postalCode": "12345"} }
     	When I use HMAC and POST to /charges?type=sale
		Then response code should be 400
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more
		And response body path $.code should be 400000 	
        And response body path $.detail should be InvalidRequestData : request.Billing: State is required
		
	@SAGEACHAPI-NBSpec-Errors-InvalidZip 
        Scenario: SAGE ACH API - NB Spec - Errors - Invalid Zip
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionClass": "PPD", "amounts":{ "total": 100.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address":  "1234 street","city": "cityville","state": "va"} }
     	When I use HMAC and POST to /charges?type=sale
		Then response code should be 400
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more
		And response body path $.code should be 400000 	
        And response body path $.detail should be InvalidRequestData : request.Billing: PostalCode is required
		
	@SAGEACHAPI-NBSpec-Errors-InvalidCountry 
        Scenario: SAGE ACH API - NB Spec - Errors - Invalid Country 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {     "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "001111111111",         "fax": "00111111"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": "@#$@#"     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": ""     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {         "token": "2a9e926e052b405bb3c71565f4d2d616",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
     	When I use HMAC and POST to /charges?type=sale
		Then response code should be 400
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more
		And response body path $.code should be 400000 	
        And response body path $.detail should be The field Country must match the regular expression	

     @SAGEACHAPI-NBSpec-Errors-InvalidEmail 
        Scenario: SAGE ACH API - NB Spec - Errors - Invalid Email 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {     "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "testcom",         "telephone": "001111111111",         "fax": "00111111"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": ""     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {         "token": "2a9e926e052b405bb3c71565f4d2d616",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
     	When I use HMAC and POST to /charges?type=sale
		Then response code should be 400
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more
		And response body path $.code should be 400000 	
        And response body path $.detail should be The Email field is not a valid e-mail address	
		
	@SAGEACHAPI-NBSpec-Errors-InvalidTelephone 
        Scenario: SAGE ACH API - NB Spec - Errors - Invalid Telephone 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {     "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "123##123!@#$",         "fax": "00111111"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": ""     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {         "token": "2a9e926e052b405bb3c71565f4d2d616",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
     	When I use HMAC and POST to /charges?type=sale
		Then response code should be 400
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more
		And response body path $.code should be 400000 	
        And response body path $.detail should be The field Telephone must match the regular expression	
		
	 @SAGEACHAPI-NBSpec-Errors-InvalidFax 
        Scenario: SAGE ACH API - NB Spec - Errors - Invalid Fax 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {     "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "1231231",         "fax": "123##123!@#$"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": ""     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {         "token": "2a9e926e052b405bb3c71565f4d2d616",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
     	When I use HMAC and POST to /charges?type=sale
		Then response code should be 400
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more
		And response body path $.code should be 400000 	
        And response body path $.detail should be The field Fax must match the regular expression	
	
	
	#---------------------------Shipping------------------------------	
	 @SAGEACHAPI-NBSpec-Errors-InvalidShippingAddressName 
        Scenario: SAGE ACH API - NB Spec - Errors - Invalid Shipping address Name 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
     	And I set body to {     "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "1231231",         "fax": "123434"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": ""     },     "shipping": {         "name": "SName!@#$",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {         "token": "2a9e926e052b405bb3c71565f4d2d616",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
		When I use HMAC and POST to /charges?type=sale
		Then response code should be 400
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more
		And response body path $.code should be 400000 	
        And response body path $.detail should be The field Name must match the regular expression	
		
	 @SAGEACHAPI-NBSpec-Errors-InvalidShippingAddress 
        Scenario: SAGE ACH API - NB Spec - Errors - Invalid Shipping Address 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
     	And I set body to {     "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "1231231",         "fax": "123434"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": ""     },     "shipping": {         "name": "SName",         "address": "SAddress!@#$",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {         "token": "2a9e926e052b405bb3c71565f4d2d616",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
		When I use HMAC and POST to /charges?type=sale
		Then response code should be 400
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more
		And response body path $.code should be 400000 	
        And response body path $.detail should be The field Address must match the regular expression	

	 @SAGEACHAPI-NBSpec-Errors-InvalidShippingCity 
        Scenario: SAGE ACH API - NB Spec - Errors - Invalid Shipping City 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
     	And I set body to {     "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "1231231",         "fax": "123434"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": ""     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity!@#$",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {         "token": "2a9e926e052b405bb3c71565f4d2d616",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
		When I use HMAC and POST to /charges?type=sale
		Then response code should be 400
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more
		And response body path $.code should be 400000 	
        And response body path $.detail should be The field City must match the regular expression	
		
	 @SAGEACHAPI-NBSpec-Errors-InvalidShippingState  
        Scenario: SAGE ACH API - NB Spec - Errors - Invalid Shipping State
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
     	And I set body to {     "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "1231231",         "fax": "123434"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": ""     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia!@#$",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {         "token": "2a9e926e052b405bb3c71565f4d2d616",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
		When I use HMAC and POST to /charges?type=sale
		Then response code should be 400
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more
		And response body path $.code should be 400000 	
        And response body path $.detail should be The field State must match the regular expression	
		
	 @SAGEACHAPI-NBSpec-Errors-InvalidShippingCountry 
        Scenario: SAGE ACH API - NB Spec - Errors - Invalid Shipping Country 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
     	And I set body to {     "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "1231231",         "fax": "123434"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": ""     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA!@#$"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {         "token": "2a9e926e052b405bb3c71565f4d2d616",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
		When I use HMAC and POST to /charges?type=sale
		Then response code should be 400		
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more
		And response body path $.code should be 400000 	
        And response body path $.detail should be The field Country must match the regular expression	
		
	 @SAGEACHAPI-NBSpec-Errors-InvalidShippingZip  
        Scenario: SAGE ACH API - NB Spec - Errors - Invalid Shipping Zip 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
     	And I set body to {     "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "1231231",         "fax": "123434"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": ""     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011!@#$",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {         "token": "2a9e926e052b405bb3c71565f4d2d616",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
		When I use HMAC and POST to /charges?type=sale
		Then response code should be 400
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more
		And response body path $.code should be 400000 	
        And response body path $.detail should be The field PostalCode must match the regular expression	
		
	 